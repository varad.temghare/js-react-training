import Child from "./Child";
import './Parent.css';

const Parent=()=>{

    let userData=[{Name :"Rahul", Phone: "900000000", Address:"Kolkata"},
                 {Name :"Sagar", Phone: "100000000", Address:"Mumbai"}]
    // let userData="abc";
    return(
        <>
            <div id="parent-div">
                {/* {userData} */}
                <Child userData={userData}/>
            </div>
        </>
    );
}

export default Parent;