import React from "react";
import './Test2.css';

const Test2 = () =>{
    const hello = () => {
        console.log('Hi');
    }

    const handleChange=(event)=>{
        console.log('name-',event.target.name);
        console.log('value-',event.target.value);
    }
    const handleChangePassword=(event)=>{
        console.log('password-',event.target.password);
        console.log('value-',event.target.value);
    }
    return(
    <>
    <div className="main1">
        <div id="Box">
            <a href="http://localhost:3000">Login</a>
            <a href="http://localhost:3000/about">About</a>
            <a href="http://localhost:3000/contact">Contact</a>
            </div>
        </div><div className="main2">
                <div className="input">
                    <label >UserID</label>
                    <input type="text" name="username" id="user" onChange={handleChange}></input>
                </div>
                <div className="input">
                    <label >Password</label>
                    <input type="password" name="password" id="password" onChange={handleChangePassword}></input>
                </div>
                <button type="button" onClick={hello} id="button">Login</button>
            </div></>
    )
}
export default Test2;