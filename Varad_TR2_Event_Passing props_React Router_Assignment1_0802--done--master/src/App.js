import './App.css';
import HWRouter from './Routers/HWRouter';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HWRouter></HWRouter>
      </header>
    </div>
  );
}

export default App;